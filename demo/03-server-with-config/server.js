#!/usr/bin/nodejs

require("../../lib/easy-web-framework.js")();
var path = require("path");

option("title","T","Give a title to page.", "Page", "Welcome!", 1);
option("main-text","m","Give a main text to page.", "Page", "Have a nice day. ", 1);
option("footnote","f","Give a footnote to page.", "Page", "Copyright by all! ", 1);

get("/", function(result, options) {
    result("<html><body><h1>"+options["title"]+"</h1><p>"+options["main-text"]+"</p><i>"+options["footnote"]+"</i></body></html>");
});

var server_config_path =path.resolve(__dirname+"/config.json");


start(server_config_path, function () {
    debug(server_config_path);
    return true;
});


