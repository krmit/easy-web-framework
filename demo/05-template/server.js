#!/usr/bin/nodejs

require("../../lib/easy-web-framework.js")();

option("title","T","Give a title to page.", "Page", "Welcome!", 1);
option("main-text","m","Give a main text to page.", "Page", "Have a nice day. ", 1);
option("footnote","f","Give a footnote to page.", "Page", "Copyright by all! ", 1);

get("/", function(result) {
    template(options,"index.html", function(page) {
        result(page);
    });
});

start({}, function () {});
