#!/usr/bin/nodejs

require("../../lib/easy-web-framework.js")();

option("message","m","Give the hello messages.", "Server", "Hello World!", 1);

get("/", function(result, options) {
    result(options["message"]);
});

start({"message":"hello world!"},function () {
    return true;
});


