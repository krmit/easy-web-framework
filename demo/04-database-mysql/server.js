#!/usr/bin/nodejs

require("../../lib/easy-web-framework.js")();
var path = require("path");

get("/create", function(result, options) {
    query("CREATE TABLE `abbreviation` ( "+
             "`id` INT NOT NULL AUTO_INCREMENT , "+
             "`abbreviation` VARCHAR(10) NOT NULL , "+
             "`phrase` TEXT NOT NULL , `explanation` MEDIUMTEXT NOT NULL , "+
             "`frequency` INT NOT NULL , PRIMARY KEY (`id`))");
    query("INSERT INTO `abbreviation` (`id`, `abbreviation`, `phrase`, "+
          "`explanation`, `frequency`) VALUES (NULL, 'lol', 'laugh out loud'"+
          ", '', '0')");
    query("INSERT INTO `abbreviation` (`id`, `abbreviation`, `phrase`, "+
          "`explanation`, `frequency`) VALUES (NULL, 'it', "+
          "'Information Technology', '', '0')");
    query("INSERT INTO `abbreviation` (`id`, `abbreviation`, `phrase`, "+
          "`explanation`, `frequency`) VALUES (NULL, 'www', 'World Wide Web'"+
          ", '', '0')");
    result("<html><body><h1>OK!</h1><p>Database is working</p></html>");
});

get("/drop", function(result, options) {
    query("DROP TABLE abbreviation");
    result("<html><body><h1>OK!</h1><p>Database is empty</p></html>");
});

get("/", function(result, options) {
    result("<html><title>Abbreviations</title><body>"+
           "<h1>Question about abbreviation.</h1><form action='/answer'>"+
           "Abbreviation:<br /><input type='text' name='abbreviation'><br />"+
           "<input type='submit' value='Submit'>"+
           "</form><a href='/create'> Create Database</a><br />"+
           "<a href='/drop'> Drop Database</a>"+
           "</html>");
});

get("/answer", function(result, options) {
    query("SELECT * FROM abbreviation WHERE abbreviation='"+
          options.param["abbreviation"]+"';", function (answer) {
        if(answer) {
            result("<html><title>Abbreviations</title><body>"+
           "<h1>Abbreviation is:</h1>"+
           "<h2>"+answer[0]["phrase"]+"</h2>"+
           "<p>This question has been asked "+answer[0]["frequency"]+
           " times. </p>"+
           "<form action='/'>"+
           "<input type='submit' value='Submit'>"+
           "</form></html>");
            query("UPDATE abbreviation SET frequency = frequency + 1 WHERE id='"
            +answer[0]["id"]+"';");
        }
        else
    {
            result("<html><title>Abbreviations</title><body>"+
           "<h1>Abbreviation is:</h1>"+
           "<h2>Your question hade no answer.</h2>"+
           "<form action='/'>"+
           "<input type='submit' value='Submit'>"+
           "</form></html>");
        }
    });
});

var server_config_path =path.resolve(__dirname+"/config.json");


start({}, function () {
    debug(server_config_path);
    info("You will need to have mysql install.");
    info("And give information about your database.");
    return true;
});
