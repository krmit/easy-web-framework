#!/usr/bin/nodejs

require("../../lib/easy-web-framework.js")();

option("title","T","Give a title to page.", "Page", "Welcome!", 1);

get("/", function(result, options) {
    var session = options.session;
    if (typeof session.views !== "undefined") {
        debug("Page view: "+session.views);
        session.views++;
    }
    else {
        debug("Create counter for views.");
        session.views=0;
    }
    template(options,"index.html", function(page) {
        result(page);
    });
});

start({}, function () {});
