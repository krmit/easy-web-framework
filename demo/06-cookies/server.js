#!/usr/bin/nodejs

require("../../lib/easy-web-framework.js")();

option("title","T","Give a title to page.", "Page", "Welcome!", 1);

get("/", function(result) {
    var set_cookies={};

    if("undefined" !== typeof options.param  && 
       "undefined" !== typeof options.param.key  &&
       "undefined" !== typeof options.param.value) {
        debug(options.param.key);
        debug(options.param.value);
        set_cookies[options.param.key] = options.param.value;
    }
    template(options,"index.html", function(page) {
        result(page, set_cookies);
    });
});

start({}, function () {});
