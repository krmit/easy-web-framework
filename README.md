# easy-web-framework

***This software is depricated, do not use it.***

***It may be rewritten next year (2020), but I promise nothing***

This is a simple and easy to use web framework, for learning some basic uses of a web server. It will try to let the web server developer only write the most important code and fix everything else automatically.

> This framework could be used to test your ideas or to learn some basics of programing a web server, but consider rewriting your web server if you want to use it for something else. See below for a list of helpful packages that we are using in this web framework.

Have fun!

## Usage

Add this package to “dependencies” in your package.json file. Then install it with the following command:

     npm install

Create a new file for your web server and add the following line:

````javascript
require(“easy-web-framework”)();
````

Then use the global define router functions or the options functions to add behavior to the server. You can use the aiding functions described below for logging and database management.

A the end of the file written:

````javascript
start(serverCallback)
````
The `serverCallback` function should be declared as follow:

````javascript
function () {
  // Your code here, will be run before server start.
}
````

Observe that the start function could take various configurations as parameters.

````javascript
start({“port”:1337}, serverCallback);
````

Or use a path to a json file containing the configuration.

````javascript
start(“relative/path/to/config.json”, serverCallback);
````

Start the server by:

      nodejs server.js

To stop the server use **Ctrl-c** on Linux, Mac or other POSIX system.

You can also add option to the server to configure it. To see which options that exist use following command:
 
      nodejs server.js -h

The options can also be used to configure the program, see above.
Nice to have options is:
   *  **-p** To set the port for the server.
   *  **-v** Display usefull information about the web server.
   *  **-d** Display debug information.

## Option functions
### option(name, short, description, default,number_of_arguments, group)

Define an option for the program. The options will be found in a object called `options`.

## Router functions
### get(url, webCallback)

This function will listen to any requst for `url` and then call the `webCallback` function. Which will have this form:

````javascript
function (result, options) {
    // Your code
  result(“What you will send back to the client.”);
}
````

The  `options.param` argument  is set to the web parameters send by the client.
The  `options.url` argument  is set to the url used to request the function.

## Aiding functions

### query(sql, queryCallback)

This function will do a query against a database if a database connection is defined by configurations or options to the server. The `queryCallback` function should be declared as follows:

````javascript
function (answer) {
  // Your code here.
}
````

The  `answer` argument  is a list of rows from the query. If the query fail for any reason the answer have the value of **false**. 

### template(data, filePath, templateCallback)

This function will read a file from `filePath` and render it with the [mustache](https://www.npmjs.com/package/mustache) template system using `data`. The `templateCallback` function should be declared as follows:

````javascript
function(page) {
  // Your code here.
}
````

The `page` argument is a string containg the result of the rendering from data and file. 

### trace(text)
### debug(text)
### info(text)
### log(text)
### warning(text)
### error(text)
### fatal(text)

A logger that will log a `text` depending on log level and message type.

## Tests

**Coming soon.**

## Contributing

Suggestions, comments and pull request would be very welcome.

## Package

   * **express** for web server.
   * **mustache** for template.
   * **yargs** for options parsing.
   * **mysql** for connecting to a mysql database.

## License

Copyright © 2017-2019 by Magnus Kronnäs, you can use it under the GNU General Public License Version 3 (GPL). The license is for you own protection so you do not do anything stupid.

## Release History
This list will not contain information about releases with only small bug fixes.

  * 0.1.0 _2019-06-22_ Fixed a bug with mariadb for me, but it could break things on other systems. Marked the software as depricated.
  * 0.0.5              Added cookies, session and escaping
  * 0.0.3              Added template functions and more options.
  * 0.0.1              Initial release
