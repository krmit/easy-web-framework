/*
 *   Copyright 2017 Magnus Kronnäs
 *
 *   This file is part of easy-web-framework.
 *
 *   easy-web-framework is free software: you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software 
 *   Foundation, either version 3 of the License, or (at your option) any later 
 *    version.
 *
 *   easy-web-framework is distributed in the hope that it will be useful, but WITHOUT ANY 
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along with 
 *   easy-web-framework. If not, see http://www.gnu.org/licenses/.
 */

var colors = require("colors");
var pretty= require("js-object-pretty-print").pretty;

function logger(level) {

    var log_print = function(tag, msg) {
        var tag_color = function(msg){return msg;};
        var msg_color = function(msg){return msg;};

        switch(tag) {
        case "fatal":
        case "error":
            tag_color = colors.red;
            msg_color = colors.blue;
            break;
        case "warn":
            tag_color = colors.yellow;
            msg_color = colors.blue;
            break;
        default:
            tag_color = colors.green;
        }

        if(typeof msg === "string" || typeof msg === "boolean" || typeof msg === "number") {
            console.log(tag_color(tag) + ": " + msg_color(msg));
        }
        else  {
            // Needed good pretty printing of json and object because of circular objects.
            console.log(tag_color(tag) + ": \n" + msg_color(pretty(msg)));
        }
    };


    trace = function(msg) {};
    if(level > 6) {
        trace = function (msg) {log_print("trace", msg);};
    }

    debug = function(msg) {};
    if(level > 5) {
        debug = function (msg) {log_print("debug", msg);};
    }

    info = function(msg) {};
    if(level > 4) {
        info = function (msg) {log_print("info", msg);};
    }

    log = function(msg) {};
    if(level > 3) {
        log = function (msg) {console.log(msg);};
    }

    warn = function(msg) {};
    if(level > 2) {
        warn = function (msg) {log_print("warn", msg);};
    }

    error = function(msg) {};
    if(level > 1) {
        error = function (msg) {log_print("error", msg);};
    }

    fatal = function(msg) {};
    if(level > 0) {
        fatal = function (msg) {log_print("fatal", msg);};
    }
}

module.exports = logger;
