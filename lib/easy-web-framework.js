/*
 *   Copyright 2017 Magnus Kronnäs
 *
 *   This file is part of easy-web-framework.
 *
 *   easy-web-framework is free software: you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software 
 *   Foundation, either version 3 of the License, or (at your option) any later 
 *    version.
 *
 *   easy-web-framework is distributed in the hope that it will be useful, but WITHOUT ANY 
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along with 
 *   easy-web-framework. If not, see http://www.gnu.org/licenses/.
 */

var fs = require("fs");
var path = require("path");
var expandHomeDir = require("expand-home-dir");
var express = require("express");
var cookieParser = require("cookie-parser");
var session = require("express-session");
var moment = require("moment");
var mustache = require("mustache");
var mysql = require("mysql");
var exec = require("child_process").exec;
var sqlString = require("sqlstring");

var error_port; //To remember witch port server connects to in case of error.

// ewf for easy-web-framework
var ewf = function() {
    ewf.arg = require("./option.js");
    option =  ewf.arg.option;
    ewf.serverName = "server.js";
    ewf.database=null;
    ewf.app = express();
     
    // Adding middleware 
    //Needed for us to use cookies
    ewf.app.use(cookieParser("No secret"));
    
    // Needed for session 
    ewf.app.use(session({
        secret: "No secret",
        resave: false,
        saveUninitialized: true,
        cookie: { 
            secure: false, 
            maxAge: 3600000 
        }}));

    option("verbose","v","Print more infromation about what happens.", "More info", false, 0);
    option("debug","d","Print debug information.", "More info", false, 0);
    option("trace","t","Print a lot of debug information.", "More info", false, 0);
    option("log-level","l","Set log level information, could be value bethween 0 and 7.", "More info", 4, 1);
    option("silent","s","Do not print any log information.", "More info", false, 0);
    option("port","p","Witch port the server will lissen to.", "Server", 44300, 1);
    option("maxAge","A","Setting the expiry time for a cookie, by adding the current time with option value in seconds.", "Server", 3600, 1);
    option("no-server","n","The server will not be started.", "Server", false, 0);
    option("base-url","B","The base url that should be used by the server.", "Server", "", 1);
    option("host","o","Give location of host.", "Database", "localhost", 1);
    option("user","u","Give user name to database server.", "Database", null, 1);
    option("password","a","Give password to database server", "Database", null, 1);
    option("database","b","Give name of database to select", "Database", null, 1);
    option("database-port","P","Give a port number or socket for connection to database", "Database", "/var/run/mysqld/mysqld.sock", 1);

    get = function(url, webCallback) {
        ewf.app.get(url, function (request, respons) {
            debug(url);
            log("["+moment().format("YYYY-MM-DD HH:mm:ss")+"] "+
                request.ip+" "+request.protocol + " " + request.method + " " 
                +request.originalUrl+"       \t\""+request.headers["user-agent"]+"\"");
          
            // Adding cookies to option
            var cookie_list=[];

            options.cookies=request.cookies;
            for(var key in options.cookies) {
                cookie_list.push({"key":key, "value": options.cookies[key]});
            }
            options["cookieList"]=cookie_list;

            // Adding cookies to option
            options.session=request.session;
            //debug(request);

            // Setting options values
            options.url=request.baseUrl;
            options.param=request.query;

            this.template(options,path.join(__dirname,"..","data","cookies-page.html"), function (page) {
                options["cookies-page"]=page;

                webCallback(function (result, setCookies) {
                    trace(result);
                    trace(setCookies);
                    if(typeof result === "number") { 
                        result=result.toString();
                    }
                    var key;
                    for(key in setCookies) {
                        respons.cookie(key, setCookies[key], { maxAge: (options["maxAge"]*1000), httpOnly: true });
                    }
                    respons.send(result);
                }, options);
            });
        });
    };

    start = function(config, callback) {
        // Check if config is not an object but a string, if so read config from file.
        if(typeof(config) === "string") {
            var config_path = path.resolve(expandHomeDir(config));
            config = JSON.parse(fs.readFileSync(config_path).toString());
        }

        // Parse options
        var cli_options=ewf.arg.yargs.epilog("Copyright 2017 by Magnus Kronnäs")
        .usage("Usage: " + ewf.serverName +" [options]")
        .alias("h", "help")
        .help("h")
        .group("help", "More info")
        .argv;
  
        // Combining defaults, options and configurations
        options=ewf.arg.defaults;
        var key;

        if(config !== undefined) {
            for (key in config) { 
                if(config[key] !== undefined) {
                    options[key] = config[key];
                } 
            }
        }
        for (key in cli_options) { 
            if(cli_options[key] !== undefined && 
               cli_options[key] !== false){
                options[key] = cli_options[key];
            }
        }

        // Create loging functions
        var level = options["log-level"];
        if(options["silent"]){
            level=0;
        }
        else if(options["verbose"]){
            level=5;
        }
        else if(options["debug"]){
            level=6;
        } else if(options["trace"]){
            level=7;
        }
        require("./logger.js")(level);
        debug(options);
        
        // Connect to database.
        
        trace(options["host"]+" "+options["user"]+" "+options["password"]+" "+
              options["database"]);
        if(typeof options["host"] === "string" && 
           typeof options["user"] === "string" && 
           typeof options["password"] === "string" && 
           typeof options["database"] === "string") {
            info("Connecting to mysql database on "+ options["host"]);
            ewf.database = mysql.createConnection({
                "host":options["host"], 
                "user":options["user"], 
                "password":options["password"],
                "database":options["database"],
                "port":options["database-port"]
            });
            info("Selected database: "+ options["database"]);
        }
        
        // Read in constans
        this.template(options,path.join(__dirname,"..","data","html-beginning.html"), function (page) {
            options["begin-page"]=page;
        });

        // Starting server
        if(callback !== undefined  && callback()!== false && 
           options["no-server"] === false) {
            info("Starting server ...");
            info("on port: " + options["port"]);
            error_port = options["port"];
            ewf.app.use(function (err, req, res, next) {
                error(err.stack);
                res.status(500).send("Your app has an error!");
            });

            ewf.app.listen(options["port"], function () {
                log((ewf.serverName+" now listning on port" +(" "+ options["port"]).blue).bold);
            });
        }
        else {
            warn("Not starting server.");
        }
    };

    query = function (sql, questionCallback) {
        debug(sql);
        ewf.database.query(sql, function (error, results, fields){
            var answer=results;
            if(error !== null || results.length === 0) {
                answer = false;
                debug(error);
            }
            trace(answer);
            if (typeof questionCallback === "function") {
                questionCallback(answer);
            }
        });
    };

    template = function (data, filePath, templateCallback) {
        var file_path = path.resolve(expandHomeDir(filePath));
        debug(file_path);
        fs.readFile(file_path, "utf8", function (err,page) {
            if (err) {
                error(err);
            }
            else {
                var result = mustache.render(page, data);
                trace(result);
                templateCallback(result);
            }
        }); 
    };

    escape = function (param) {
        return sqlString.escape(param);
    };
};

process.on("uncaughtException", function(err) {
    switch(err.errno) {
    case "EADDRINUSE":
        error("Another server is already listen to port: "
                        +error_port);
        info("Maybe you did not close your last server properly?");
        exec("lsof -i :"+error_port+" | tail -1 | tr -s ' ' | cut -d' ' -f2;", 
             function (err, stdout, stderr) {
                 if (err !== null) {
    // If os dose not have lsof or is windows.
                     info("You need to find the outher server process and close it.");
                 }
                 else {
                     var pidid = stdout.trim();
                     info("The blocking server process id is "+pidid+". This "+
                     "command will close the outher server:");
                     info("kill -9 "+pidid);
                 }
                 info("Or you could choose a new port for your server with the \"-p\""+
                 " flag.");
                 process.exit(1);
             });
        break;
    default:
        throw err;   
    }
});

module.exports = ewf;
