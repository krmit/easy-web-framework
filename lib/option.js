/*
 *   Copyright 2017 Magnus Kronnäs
 *
 *   This file is part of easy-web-framework.
 *
 *   easy-web-framework is free software: you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software 
 *   Foundation, either version 3 of the License, or (at your option) any later 
 *    version.
 *
 *   easy-web-framework is distributed in the hope that it will be useful, but WITHOUT ANY 
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along with 
 *   easy-web-framework. If not, see http://www.gnu.org/licenses/.
 */

var arg = {};
arg.yargs = require("yargs");
arg.defaults={};

arg.option = function (name, flag, description, group, default_value,number_of_arguments) {
    arg.yargs.describe(name, description);
    if(group !== "") { 
        arg.yargs.group(name, group);
    }
    if(flag !== "") { 
        arg.yargs.alias(flag, name);
    }
    if(default_value !== "") { 
        arg.defaults[name]=default_value;
    }
    if(number_of_arguments === 0) { 
        arg.yargs.boolean(name);
    }
    else {
        arg.yargs.nargs(name, number_of_arguments);
    }
};

module.exports = arg;
